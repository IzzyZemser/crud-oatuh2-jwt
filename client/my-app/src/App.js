import React, { useState, useEffect } from "react";
import styled from "styled-components";
import axios from "axios";
import logo from './logo.svg';
import './App.css';
import {FaFacebookF,FaGithub} from 'react-icons/fa';
import qs from 'query-string'


const USERS_URL = "http://localhost:3030/api/users/";
const AUTH_URL = "http://localhost:3030/api/auth/";
const hardCodedUser =   {
  "id": 112,
  "first_name": "Avi",
  "last_name": "Kozokin",
  "email": "xoxo@gmail.com",
  "country": "Israel",
  "password": "123456",
  token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEyLCJzb21lIjoib3RoZXIgdmFsdWUiLCJpYXQiOjE2MjAyMjk3OTYsImV4cCI6MTYyMDMxNjE5Nn0.XUl-plkLYBMkjetkl3wHcbudCb4vmfWTy7HRQyd-I0g",
  avater:"https://avatars.githubusercontent.com/u/44840990?v=4"
};


const App = () => {
  const [logged, set_logged] = useState(false);
  // const [loading,set_loading] = useState(false);
  const [user,set_user] = useState(null);
  const [access_token,set_access_token] = useState(null);
  const [content,set_content] = useState('no content yet...');

  

  const authReq = (provider) => {
    window.location.href = `${AUTH_URL}${provider}`; 
  }

  const loginReq = async () => {
    let headers = {"x-access-token" : token}
    const {email, password, token, avater} = hardCodedUser;
    try{
      const res = await axios.post(`${AUTH_URL}login`, {email, password}, {headers})
      const {user} = res.data
      set_user(user)
      set_logged(true);
    }catch(err){
      console.log("login failed!")
    }
  }

  const logoutReq = async () => {
    try{
      const response = await axios.get(`${AUTH_URL}logout`)
      console.log(response)
      if(response.data.status === 'You are logged out'){
        set_logged(false)
        set_user(null)
        set_access_token(null)
      }
    }catch(err){
      console.log("login failed!")
    } 
  }

  const protectedRouteReq = async () => {
    try{
      let headers = {"x-access-token" : access_token}
      let response = await axios.get(`${USERS_URL}secret`, {headers})
      console.log(response)
      set_content(response.data.msg)
    }catch(err){
      console.log("not authorized")
    }
   
  }

  const getAccessToken = async () => {
    try{
      const response =  await axios.get(`${AUTH_URL}get-access-token`)
      if('access_token' in response && 'profile' in response){
        set_access_token(response.access_token);
        set_logged(true);
        console.log(JSON.parse(response.profile))
        set_user(JSON.parse(response.profile))
      }else{
        alert('please login')
      }
    }catch(err){
      console.log("not authorized")
    }
   
  }


  useEffect(()=>{
    if(window.location.search && window.location.search.includes('token')){
      console.log('search - ',window.location.search)
      const search = qs.parse(window.location.search);
      console.log('qs - ',search)
      const profile = JSON.parse(search.profile)
      const {token} = search
      console.log('profile - ',profile)
      console.log('token - ',token)
      if(profile && token){
        set_logged(true)
       set_user(profile)
       set_access_token(token) //store access_token in-memory
       window.history.replaceState({}, document.title, "/");
      }
    }else if(!access_token){
      getAccessToken()
    }
  },[access_token])

  return (
    <AppDiv>
      <div className="header">
        <h1>Auth Demo</h1>
        <img src={logo} className="App-logo" alt="logo" />
      </div>
      <Box>
        <h3>Authenticated: <span>{logged? "true" : "false"}</span></h3>
        <h3><span>user:</span></h3>
        <div className="userDisplay">
        </div>
            {user && <>
            <p>{user.name}</p>
            <img src={user && user.photo} alt="avatar" />
            </>}
        <div>
          {logged ? <button onClick={logoutReq}>Logout</button> : 
          <div>
            <div>
              <button onClick={()=>authReq('github')}><FaGithub/></button>
              <button onClick={()=>authReq('facebook')}><FaFacebookF/></button>
            </div>
            <form onSubmit={loginReq}>
              <label>Email:</label><input type="email"/>
              <label>Password:</label><input type="password"/>
              <input type="submit"/>
            </form>
          </div>}
          <button onClick={protectedRouteReq}>Get protected content</button>
        </div>
        <p>{content}</p>
      </Box>
    </AppDiv>
  );
};

export default App;



const AppDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items:center; 
  justify-content:center;
  margin-top: 5rem;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
  height:100vh;
  color:white;
  
  .header h1{
    font-size: 3rem;
  }
  h1{
    color:white;
    font-size:10px;

  }
  .header{
    display:flex;
    align-items: center;
  }
`;

const Box = styled.div`
  display: flex;
  flex-direction: column;
  justify-content:center;
  align-items:center; 
  color:white;
  
  span{
    color: #61dafb;
  }
  button{
    background-color: #61dafb;
    padding:10px;
    border-radius:5px;
  }
  form{
    display:flex;
    flex-direction:column;
    margin-top:10px;
    input{
      padding: 0.2rem;
    }
    input:last-child{
      margin-top:10px;
      margin-bottom:10px;
    }
  }
  & > * {
    margin: 1rem;;
  }
`;
