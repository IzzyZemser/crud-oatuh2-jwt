import express from 'express'
import passport from 'passport'
import jwt from 'jsonwebtoken'
import ms from 'ms'
import cookieParser from 'cookie-parser'

const router = express.Router()
router.use(cookieParser())


const {CLIENT_ORIGIN, APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION} = process.env;


const githubAuth = passport.authenticate('github', { session: false })
const facebookAuth = passport.authenticate('facebook', { session: false })


export const verifyAuth = async (req, res, next) => {
    try {     
        // check header or url parameters or post parameters for token
        const access_token = req.headers['x-access-token'];
        console.log("here")
        if (!access_token) return res.status(403).json({
            status:'Unauthorized',
            payload: 'No token provided.'
        });
  
        // verifies secret and checks exp
        const decoded = await jwt.verify(access_token, APP_SECRET)
  
        // if everything is good, save to request for use in other routes
        req.user_id = decoded.id;
        next();
  
    } catch (error) {
        return res.status(401).json({
            status:'Unauthorized',
            payload: 'Unauthorized - Failed to authenticate token.'
        });
    }
  }


  function redirect_tokens(req,res,user){
    const access_token = jwt.sign({ id : req.user.id , some:'other value'}, APP_SECRET, {
      expiresIn: ACCESS_TOKEN_EXPIRATION // expires in 1 minute
    })
    const refresh_token = jwt.sign({ id : req.user.id , profile:JSON.stringify(user)}, APP_SECRET, {
      expiresIn: REFRESH_TOKEN_EXPIRATION // expires in 60 days... long-term... 
    })
    res.cookie('refresh_token',refresh_token, {
      maxAge: ms('60d'), //60 days
      httpOnly: true
    })
    res.redirect(`${CLIENT_ORIGIN}?token=${access_token}&profile=${encodeURIComponent(JSON.stringify(user))}`)
}

router.get('/github', githubAuth)

router.get('/error', (req, res) => res.send('Unknown Error'))

router.get('/github/callback', githubAuth , (req, res) => {
    const user = { 
      name: req.user.username,
      photo: req.user.photos[0].value
    }
    console.log("here")
    redirect_tokens(req,res,user)
})

router.get('/facebook', facebookAuth)

router.get('/facebook/callback', facebookAuth, (req, res) => {
  const { givenName, familyName } = req.user.name
  const user = { 
    name: `${givenName} ${familyName}`,
    photo: req.user.photos[0].value
  }
  redirect_tokens(req,res,user)
})

router.get('/get-access-token',async (req,res)=> {
  //get refresh_token from client - req.cookies
  const {refresh_token} = req.cookies;

  console.log({refresh_token});

  if (!refresh_token) return res.status(403).json({
      status:'Unauthorized',
      payload: 'No refresh_token provided.'
  });

  try{
    // verifies secret and checks expiration
    const decoded = await jwt.verify(refresh_token, APP_SECRET)
    console.log({decoded})

    const {id, profile} = decoded;

    const access_token = jwt.sign({ id , some:'other value'}, APP_SECRET, {
        expiresIn: ACCESS_TOKEN_EXPIRATION //expires in 1 minute
    })
    res.status(200).json({access_token, profile })
  }catch(err){
      console.log('error: ',err)
      return res.status(401).json({
          status:'Unauthorized',
          payload: 'Unauthorized - Failed to verify refresh_token.'
      });
  }   
})

router.get('/logout',(req,res)=>{
  req.logout();
  res.clearCookie('refresh_token');
  res.status(200).json({status:'You are logged out'})
})




export default router;