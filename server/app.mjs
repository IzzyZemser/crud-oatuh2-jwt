import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import log from '@ajar/marker'
import auth_router from './auth/auth.router.mjs'
import users_router from './users/users.router.mjs'
import passport from 'passport'
import passport_config from './auth/passport.config.mjs'
import connection from './db/sql.connection.mjs'

// import auth_router,{verifyAuth} from './auth/auth.router.mjs'

const { PORT,HOST } = process.env;

const app = express();

app.use(express.json());
app.use(cors());
app.use(morgan('dev'))

passport_config()
app.use(passport.initialize())
app.use(passport.session());

app.use('/api/auth/', auth_router);

app.use('/api/users', users_router);


  app.use('*', (req,res)=> {
    res.status(404).json({status:`path ${req.url} is not found`})
});


  ;(async ()=> { 
    await app.listen(PORT,HOST);
    log.magenta(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
  })().catch(log.error)