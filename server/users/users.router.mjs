import raw from "../middleware/route.async.wrapper.mjs";
import express from 'express';
import log from '@ajar/marker';
import {verifyAuth} from '../auth/auth.router.mjs'
import connection from '../db/sql.connection.mjs'
import { 
  verify_token, 
  false_response, 
  tokenize 
} from '../middleware/auth.middleware.mjs';
import bcrypt from 'bcryptjs';

const router = express.Router();


// parse json req.body on post routes
router.use(express.json())

//Get all USERs
router.get( "/",raw(async (req, res) => {
  const [rows, fields] =  await connection.query('SELECT * FROM playground.data;')
  res.status(200).json(rows);
})
);

// router.get('/logout', raw( async (req, res)=> {
//   return res.status(200).json(false_response)
// }))

// router.get('/me', verify_token, raw( async (req, res)=> {
//   const [rows, fields] =  await connection.query(`SELECT * FROM data WHERE id="${req.user_id}"; `)

//   if (rows.length === 0) return res.status(404).json({message:'No user found.'});
//   res.status(200).json(rows);
// }))

// router.get('/paginate/:page?/:items?', raw( async(req, res)=> {
//   let { page = 0 ,items = 10 } = req.params;
//   const [rows, fields] =  await connection.query(`SELECT * FROM data LIMIT ${parseInt(items)} OFFSET ${parseInt(page * items)}`)
//   res.status(200).json(rows);
// }))

router.get('/secret', verifyAuth, function (req, res, next) {
    res.json({msg: 'This is a secret message for logged in users!'})
  })

// // GETS A SINGLE USER
router.get("/:id",raw(async (req, res) => {
  const [rows, fields] =  await connection.query(`SELECT * FROM playground.users_data WHERE id=${req.params.id}`)
  if(rows.length === 0){
    return res.status(404).send("User not found");
  }else{
    res.status(200).json(rows);
  }}));

// //Add single user
// router.post("/", raw(async (req, res) => {
//     let { first_name ,last_name, email, country} = req.body;
//     const [rows, fields] =  await connection.query(`INSERT INTO data (first_name, last_name, email, country) values ("${first_name}", "${last_name}","${email}", "${country}");`)
//     res.status(200).json(rows);
// }));

// // Adds Multiple users
router.post("/add-many", raw(async (req, res) => {
  const valString = req.body.reduce((acc, cur) => {
    if(acc){
      acc += " , "
    }
    return acc + ` ("${cur.first_name}", "${cur.last_name}","${cur.email}", "${cur.country}", "${cur.password}")`
  }, "");

  const [rows, fields] =  await connection.query(`INSERT INTO users_data (first_name, last_name, email, country,  password) values ${valString};`)
    res.status(200).json(rows);
}));

// // UPDATES A SINGLE USER
// router.put("/:id",raw(async (req, res) => {
//     let valString = "";
//     for(let key of Object.keys(req.body)){
//       if(valString){
//         valString += " , ";
//       }
//       valString += ` ${key}="${req.body[key]}"`;
//     }
//     const [rows, fields] =  await connection.query(`UPDATE data SET ${valString} WHERE id=${req.params.id};
//     `)
//     res.status(200).json(rows);
// })
// );


// // DELETES A USER
// router.delete("/:id",raw(async (req, res) => {
//   const [rows, fields] =  await connection.query(`DELETE FROM data WHERE id=${req.params.id};`)
//   res.status(200).json(rows);
 
// })
// );


router.post('/register', raw( async (req, res)=> {
    
  log.obj(req.body,'register, req.body:')

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);
  
  log.info('hashedPassword:',hashedPassword)
  const user_data = {
      ...req.body,
      password : hashedPassword
  }
  // create a user
  const [rows, fields] =  await connection.query(`INSERT INTO users_data (first_name, last_name, email, country, password) values ("${user_data.first_name}", "${user_data.last_name}","${user_data.email}", "${user_data.country}", "${user_data.password}");`)
  // log.obj(rows.id,'register, created_user:') 
  
  // create a token
  const token = tokenize(rows.id) 
  log.info('token:',token)

  return res.status(200).json({ 
      auth: true, 
      token ,
      user : rows
  })
}))

router.post('/login', raw( async (req, res)=> {
  //extract from req.body the credentials the user entered
  const {email,password} = req.body;
  console.log(email, password)
  console.log(req.body)
  //look for the user in db by email
  let [rows, fields] =  await connection.query(`SELECT * FROM users_data WHERE email="${email}"; `)
  //if no user found...
  if (rows.length === 0) return res.status(401).json({...false_response,message:"wrong email or password"}) 
  console.log(rows)
  rows = rows[0]
  console.log(rows)
  // check if the password is valid
  const password_is_valid = await bcrypt.compare(password, rows.password)
  console.log(password_is_valid)
  if (!password_is_valid) return res.status(401).json({...false_response,message:"wrong email or password"})

  // if user is found and password is valid
  // create a fresh new token
  const token = tokenize(rows.id)
  // return the information including token as JSON
  return res.status(200).json({ 
      auth: true, 
      token,
      user: {id: rows.id, first_name:rows.first_name, last_name:rows.last_name, email: rows.email}
  })
}))


export default router;